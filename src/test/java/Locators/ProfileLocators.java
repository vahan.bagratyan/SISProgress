package Locators;

import org.openqa.selenium.By;

public class ProfileLocators {
    public By logOut = By.xpath("//android.view.View[@content-desc=\"Log out\"]");
    public By logOutYes = By.xpath("//android.widget.Button[@content-desc=\"Yes\"]");
}
